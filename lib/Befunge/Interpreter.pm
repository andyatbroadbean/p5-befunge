package Befunge::Interpreter;

use Moose;

my $RIGHT = 'RIGHT';
my $LEFT  = 'LEFT';
my $UP    = 'UP';
my $DOWN  = 'DOWN';

has 'grid' => (
  is => 'ro',
  isa => 'Befunge::CodeGrid',
  required => 1,
);

has 'direction' => (
  is => 'rw',
  default => $RIGHT,
);

has 'pos_x' => (
  traits => ['Counter'],
  is => 'rw',
  isa => 'Num',
  default => 0,
  handles => {
    go_right => 'inc',
    go_left  => 'dec',
  },
);

has 'pos_y' => (
  traits => ['Counter'],
  is => 'rw',
  isa => 'Num',
  default => 0,
  handles => {
    go_up   => 'dec',
    go_down => 'inc',
  },
);

has 'stack' => (
  is => 'ro',
  default => sub { []; },
);

sub run_once {
  my ($self) = @_;

  $self->run_instruction($self->current_instruction());
}

sub next_step {
  my ($self) = @_;

  my $dir = $self->direction();
  if ( $dir eq $RIGHT ) {
    $self->go_right();
  }
  elsif ( $dir eq $LEFT ) {
    $self->go_left();
  }
  elsif ( $dir eq $UP ) {
    $self->go_up();
  }
  elsif ( $dir eq $DOWN ) {
    $self->go_down();
  }
}

sub run_instruction {
  my ($self, $instruction) = @_;

  if ( $instruction eq '0' ) {
    # FIXME: handle 0-9
    return;
  }
  elsif ( $instruction eq q{"} ) {
    $self->process_string();
  }
  elsif ( $instruction eq '>' ) {
    $self->direction($RIGHT);
  }
  elsif ( $instruction eq 'v' ) {
    $self->direction($DOWN);
  }
  elsif ( $instruction eq ':' ) {
    $self->add_to_stack( $self->peek_at_stack() );
  }

}

sub process_string {
  my ($self) = @_;
  $self->next_step();
  my $current_instruction = $self->current_instruction();
  if ( $current_instruction eq q{"} ) {
    return;
  }
  $self->add_to_stack($current_instruction);
  $self->process_string();
}

sub current_instruction {
  my $self = shift;
  return $self->grid()->at( $self->current_pos );
}

sub current_pos {
  my $self = shift;
  return ($self->pos_x, $self->pos_y);
}

# STACK MANIPULATION
sub add_to_stack {
  my ($self, $value) = @_;
  push @{ $self->stack }, $value;
}

sub peek_at_stack {
  my ($self, $value) = @_;
  my $stack_ref = $self->stack;
  return $stack_ref->[$#$stack_ref];
}

__PACKAGE__->meta->make_immutable;
