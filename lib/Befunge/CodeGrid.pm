package Befunge::CodeGrid;

use Moose;

has 'code' => (
  is => 'ro',
  isa => 'Str',
  required => 1,
);

has '_grid' => (
  is => 'ro',
  isa => 'ArrayRef',
  lazy => 1,
  builder => '_build_grid',
);

sub _build_grid {
  my ($self) = @_;

  my @grid;
  foreach my $row ( split /\n/, $self->code() ) {
    my @row;
    foreach my $col ( split '', $row ) {
      push @row, $col;
    }

    push @grid, \@row;
  }

  return \@grid;
}

sub at {
  my ($self, $x, $y) = @_;
  return $self->_grid->[$y]->[$x];
}

__PACKAGE__->meta->make_immutable;
