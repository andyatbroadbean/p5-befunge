use Test::Most;

use_ok 'Befunge::CodeGrid';

my $code = <<_SRC;
0".dlrow ,olleH">v
                ,:
                ^_@
_SRC

my $grid = Befunge::CodeGrid->new( code => $code );

my $min_x = 0;
my $max_x = 18;
my $min_y = 0;
my $max_y = 2;

my %tests = (
  "Top left is 0" => {
    coords => [$min_x,$min_y],
    expect => '0',
  },
  "Top right is a space" => {
    coords => [$max_x,$min_y],
    expect => undef,
  },
  "top row, right most value is a v" => {
    coords => [$max_x-1, $min_y],
    expect => 'v',
  },
  "middle row, right most value is a colon" => {
    coords => [$max_x-1,$min_y+1],
    expect => ':',
  },
  "Bottom left is a space" => {
    coords => [$min_x,$max_y],
    expect => " ",
  },
  "Bottom right is an @" => {
    coords => [$max_x,$max_y],
    expect => '@',
  },
  "Out of the grid is a space" => {
    coords => [$max_x+1, $max_y+1],
    expect => undef,
  },
);

while ( my ($label, $test) = each %tests ) {
  is $grid->at(@{ $test->{coords} }), $test->{expect}, $label;
}

done_testing;
