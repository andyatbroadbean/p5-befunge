use Test::Most;

use Befunge::CodeGrid;

use_ok('Befunge::Interpreter');

my $code = <<_SRC;
0".dlrow ,olleH">v
                ,:
                ^_@
_SRC

my $grid = Befunge::CodeGrid->new( code => $code );

my $int = Befunge::Interpreter->new(
    grid => $grid,
);

# '0' instruction
$int->run_once();
is_deeply $int->stack, [], 'Stack is empty';

# string mode
$int->next_step();
is $int->current_instruction, q{"}, "Next instruction is a quote";
$int->run_once();
is_deeply $int->stack, [split //, qq/.dlrow ,olleH/], 'Stack contains Hello world';

# > instruction changes direction to the right
{
  $int->next_step();
  is $int->current_instruction, q{>}, "Next instruction is a >";
  my @current_stack = @{ $int->stack };
  $int->run_once();
  is_deeply $int->stack, \@current_stack, 'My stack is unchanged';
  is $int->direction(), 'RIGHT';
};

$int->next_step();
is $int->current_instruction, q{v}, "Next instruction is a v";
$int->run_once();
is $int->direction(), 'DOWN';
$int->next_step();
is $int->current_instruction, ':', "We have followed the direction down";

{
  my @current_stack = @{ $int->stack };
  $int->run_once();
  is_deeply $int->stack, [ @current_stack, "H" ], 'My stack is unchanged';
};

done_testing();
